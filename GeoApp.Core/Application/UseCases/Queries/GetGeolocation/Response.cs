namespace GeoApp.Api.Application.UseCases.Queries.GetGeolocation;

/// <summary>
/// Геолокация
/// </summary>
public class Location
{
    /// <summary>
    /// Горизонталь
    /// </summary>
    public int X { get; }

    /// <summary>
    /// Вертикаль
    /// </summary>
    public int Y { get; }

    /// <summary>
    /// Ctr
    /// </summary>
    private Location()
    {
    }

    /// <summary>
    /// Ctr
    /// </summary>
    /// <param name="x">Горизонталь</param>
    /// <param name="y">Вертикаль</param>
    public Location(int x, int y) : this()
    {
        X = x;
        Y = y;
    }

    /// <summary>
    /// Создать рандомную координату
    /// </summary>
    /// <returns>Результат</returns>
    public static Location CreateRandom()
    {
        var rnd = new Random();
        var x = rnd.Next(1, 10);
        var y = rnd.Next(1, 10);
        var location = new Location(x, y);
        return location;
    }
}