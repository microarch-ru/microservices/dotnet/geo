﻿using MediatR;

namespace GeoApp.Api.Application.UseCases.Queries.GetGeolocation
{
    /// <summary>
    /// Получить геолокацию по адресу
    /// </summary>
    public class Query : IRequest<Location>
    {
        public string Street { get; }

        public Query(string street)
        {
            if (String.IsNullOrEmpty(street)) throw new ArgumentException(nameof(street));
            Street = street;
        }
    }
}