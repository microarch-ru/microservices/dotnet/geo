﻿using MediatR;

namespace GeoApp.Api.Application.UseCases.Queries.GetGeolocation
{
    public class Handler : IRequestHandler<Query, Location>
    {
        public Handler()
        {
        }

        public async Task<Location> Handle(Query message, CancellationToken cancellationToken)
        {
            //В этом сервисе простая логика, реализуем ее прямов  UseCase, чтобы не усложнять
            switch (message.Street)
            {
                case "Тестировочная":
                    return await Task.FromResult(new Location(1, 1));
                case "Айтишная":
                    return await Task.FromResult(new Location(2, 2));
                case "Эйчарная":
                    return await Task.FromResult(new Location(3, 3));
                case "Аналитическая":
                    return await Task.FromResult(new Location(4, 4));
                case "Нагрузочная":
                    return await Task.FromResult(new Location(5, 5));
                case "Серверная":
                    return await Task.FromResult(new Location(6, 6));
                case "Мобильная":
                    return await Task.FromResult(new Location(7, 7));
                case "Бажная":
                    return await Task.FromResult(new Location(8, 8));
                default:
                    return await Task.FromResult(Location.CreateRandom());
            }
        }
    }
}