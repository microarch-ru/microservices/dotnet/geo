using GeoApp.Api.Application.UseCases.Queries.GetGeolocation;
using Grpc.Core;
using MediatR;

namespace GeoApp.Api.Adapters.Grpc
{
    public class Server : Geo.GeoBase
    {
        private readonly IMediator _mediator;

        public Server(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public override async Task<GetGeolocationReply> GetGeolocation(GetGeolocationRequest request,
            ServerCallContext context)
        {
            var getGeolocationQuery = new Query(request.Street);
            var result = await _mediator.Send(getGeolocationQuery);

            var getGeolocationReply = new GetGeolocationReply()
            {
                Location = new Location()
                {
                    X = result.X,
                    Y = result.Y
                }
            };
            return getGeolocationReply;
        }
    }
}