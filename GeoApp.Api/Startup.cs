using GeoApp.Api.Adapters.Grpc;
using GeoApp.Api.Application.UseCases.Queries.GetGeolocation;
using MediatR;

namespace GeoApp.Api
{
    public class Startup
    {
        public Startup()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables();
            var configuration = builder.Build();
            Configuration = configuration;
        }

        /// <summary>
        /// Конфигурация
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            //MediatR
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<Startup>());

            //Commands

            //Queries
            services
                .AddTransient<IRequestHandler<Query, GeoApp.Api.Application.UseCases.Queries.GetGeolocation.Location>,
                    Handler>();

            //gRPC
            services.AddGrpc();

            services.AddHealthChecks();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHealthChecks("/health");
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    var endpointDataSource = context
                        .RequestServices.GetRequiredService<EndpointDataSource>();

                    await context.Response.WriteAsJsonAsync(new
                    {
                        results = endpointDataSource
                            .Endpoints
                            .OfType<RouteEndpoint>()
                            .Where(e => e.DisplayName?.StartsWith("gRPC") == true)
                            .Select(e => new
                            {
                                name = e.DisplayName,
                                pattern = e.RoutePattern.RawText,
                                order = e.Order
                            })
                            .ToList()
                    });
                });

                endpoints.MapGrpcService<Server>();
            });
        }
    }
}